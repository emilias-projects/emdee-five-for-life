const load = new Date();

let length = 20;
const characters =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
var string = "";
const charactersLength = characters.length;
for (let i = 0; i < length; i++) {
  string += characters.charAt(Math.floor(Math.random() * charactersLength));
}

const target = document.getElementById("target");
target.innerHTML = string;

const form = document.getElementById("form");
form.addEventListener("submit", submit, true);

function submit(e) {
  e.preventDefault();
  const now = new Date();
  const logger = document.getElementById("notification");

  const providedHash = document.getElementById("md5").value;
  if (providedHash === md5(string)) {
    if (now.getTime() - load.getTime() <= 1000) {
      return (logger.innerHTML = "You got it!");
    }
    return (logger.innerHTML = "Too slow!");
  }
  return (logger.innerHTML = "Wrong Hash!");
}
