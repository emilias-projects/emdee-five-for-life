# Emdee Five For Life

This container image is inspired by [Emdee Five For Life]() by HTB (Hack the Box)

## Goal

The goal is to convert the presented string to md5 and then submit this withint 1 second.

## Setup

To get this container up and running you can use provided [docker compose](./docker/docker-compose.yml) file or the following example:

```sh
docker run -d -p 8090:80 --name md5-for-life registry.gitlab.com/emilias-projects/emdee-five-for-life:latest
```
